<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Admin extends BaseController {

  public function __construct()
  {
      parent::__construct();
      $this->isLoggedIn($this->session->userdata('loggedIn'));      
      $this->isAdmin($this->session->userdata('userType'));   
  }

  public function index()
  {
    $this->load->view('dashboard/home');

  }

} 

?>