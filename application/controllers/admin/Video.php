<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Video extends BaseController {

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));      
        $this->isAdmin($this->session->userdata('userType'));      
    }

    public function index()
    {
        $allData = $this->common->getAllData('video');

        $data=['pageName'=>"Videos",
                'action'  => 'add',
                'allData' => $allData, 
                ];

        $this->load->view('dashboard/video',$data);

    }

    public function add()
    {
		$image = $this->common->upload('image');
        
        $data = array
        (
            'date' => $_POST['date'],
            'title' => $_POST['title'],
            'link' => $_POST['link'],
            'image' => $image
        );  

        $this->db->insert('video', $data);

        $allData = $this->common->getAllData('video');

        $pageData=['pageName'=>"Videos",
                    'action'  => 'add',
                    'allData' => $allData, 
        ];

        $this->load->view('dashboard/video',$pageData);

    }

    public function delete($id)
    {

        $this->common->delete('video', $id);
    
        $allData = $this->common->getAllData('video');
    
        $pageData=['pageName'=>"Videos",
                'allData' => $allData,
                'action'  => 'add'
            ];
            
        $this->load->view('dashboard/video', $pageData); 
    
    }
    
    public function loadUpdate($id){
    
        $updateData = $this->common->getById('video',$id);
    
        $allData = $this->common->getAllData('video');
    
        $pageData=['pageName'=>"Videos",
                    'allData' => $allData,
                    'updateData'  => $updateData,
                    'action'  => 'update',
                ];
    
        $this->load->view('dashboard/video', $pageData);
    
    }
    
    public function update($id){
        
        $data = array
        (
            'date' => $_POST['date'],
            'title' => $_POST['title'],
            'link' => $_POST['link']
        );
        
        if(!empty($_FILES['image']['name'])){
			$image = $this->common->upload('image');
			$data['image']=$image;
		}
    
        $this->common->update('video', $id, $data);
        $allData = $this->common->getAllData('video');
    
        $pageData=['pageName'=>"Videos",
            'allData' => $allData,
            'action'  => 'add'
        ];
    
        $this->load->view('dashboard/video', $pageData);
    
    }


} 

?>