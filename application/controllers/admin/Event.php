<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Event extends BaseController {

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));      
        $this->isAdmin($this->session->userdata('userType'));      
    }

    public function index()
    {
        $allData = $this->common->getAllData('event');

        $data=['pageName'=>"Events",
                'action'  => 'add',
                'allData' => $allData, 
                ];

        $this->load->view('dashboard/event',$data);

    }

    public function add()
    {
		$image = $this->common->upload('image');
        
        $data = array
        (
            'date' => $_POST['date'],
            'title' => $_POST['title'],
            'description' => $_POST['description'],
            'place' => $_POST['place'],
            'image' => $image
        );  

        $this->db->insert('event', $data);

        $allData = $this->common->getAllData('event');

        $pageData=['pageName'=>"Events",
                    'action'  => 'add',
                    'allData' => $allData, 
        ];

        $this->load->view('dashboard/event',$pageData);

    }

    public function delete($id)
    {

        $this->common->delete('event', $id);
    
        $allData = $this->common->getAllData('event');
    
        $pageData=['pageName'=>"Events",
                'allData' => $allData,
                'action'  => 'add'
            ];
            
        $this->load->view('dashboard/event', $pageData); 
    
    }
    
    public function loadUpdate($id){
    
        $updateData = $this->common->getById('event',$id);
    
        $allData = $this->common->getAllData('event');
    
        $pageData=['pageName'=>"Events",
                    'allData' => $allData,
                    'updateData'  => $updateData,
                    'action'  => 'update',
                ];
    
        $this->load->view('dashboard/event', $pageData);
    
    }
    
    public function update($id){
        
        $data = array
        (
            'date' => $_POST['date'],
            'title' => $_POST['title'],
            'description' => $_POST['description'],
            'place' => $_POST['place']
        );
        
        if(!empty($_FILES['image']['name'])){
			$image = $this->common->upload('image');
			$data['image']=$image;
		}
    
        $this->common->update('event', $id, $data);
        $allData = $this->common->getAllData('event');
    
        $pageData=['pageName'=>"Events",
            'allData' => $allData,
            'action'  => 'add'
        ];
    
        $this->load->view('dashboard/event', $pageData);
    
    }


} 

?>