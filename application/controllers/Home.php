<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';

class Home extends CI_Controller {

    public function index()
	{ 
        $data['pageName']="Home";
        // $data['events'] = $this->common->getDataLimit("event", 5);
        // $data['videos'] = $this->common->getDataLimit("video", 3);
        // testing version control
        // texting success

        // clickup update test
		$this->load->view('home', $data);
    }
    
    // public function about()
	// { 
    //     $data['pageName']="About Us";
        
	// 	$this->load->view('about', $data);
    // }
    
    // public function video()
	// { 
    //     $data['pageName']="Videos";
    //     $data['mainVideo'] = $this->common->getDataLimit("video", 1);
    //     $data['videos'] = $this->common->getDataLimit("video", 10);
        
	// 	$this->load->view('video', $data);
    // }
    
    // public function contact()
	// { 
    //     $data['pageName']="Contact Us";
        
	// 	$this->load->view('contact', $data);
    // }

}