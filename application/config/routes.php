<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['home'] = 'home';
$route['about'] = 'home/about';
$route['video'] = 'home/video';
$route['contact'] = 'home/contact';
$route['login'] = 'login';
$route['checkLogin'] = 'login/checkLogin';
$route['logout'] = 'login/logout';

// admin route
$route['admin'] = 'admin/admin';
$route['admin/event'] = 'admin/event';
$route['admin/event/add'] = 'admin/event/add';
$route['admin/event/delete/(:num)'] = 'admin/event/delete/$1';
$route['admin/event/loadUpdate/(:num)'] = 'admin/event/loadUpdate/$1';
$route['admin/video'] = 'admin/video';
$route['admin/video/add'] = 'admin/video/add';
$route['admin/video/delete/(:num)'] = 'admin/video/delete/$1';
$route['admin/video/loadUpdate/(:num)'] = 'admin/video/loadUpdate/$1';