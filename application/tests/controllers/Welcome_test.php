<?php
/**
 * Part of ci-phpunit-test
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/ci-phpunit-test
 */

class Welcome_test extends TestCase
{
	public function test_price()
	{
		$inputs = [5, 6, 7, 8];
		$expecteds = ['300.00', '400.00', '100.00', '300.00'];

		$x=0;
		while($x < count($inputs)) {
			$actual = $this->request('GET', ['Test', 'price', $inputs[$x]]);
			$expected = $expecteds[$x];
			$this->assertEquals($expected, $actual);
			$x++;
		}
		
	}

	
}
