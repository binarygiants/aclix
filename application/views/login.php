<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<!-- Event Section Begin -->
<section class="event spad" style="margin-bottom: 500px; margin-top: 150px">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-12 m-auto">
                <div class="my__account__wrapper">
                    <h3 class="account__title text-center">Login</h3>
                    <?php if($message):?><div class="error text-center"><?php echo $message; ?></div><?php endif; ?>
                    <form action="<?php echo base_url('checkLogin') ?>" method="post">
                        <div class="account__form text-center">
                            <div class="input__box mt-3">
                                <label>Email<span>*</span></label>
                                <input type="email" class="form-control" name="email" required>
                            </div>
                            <div class="input__box mt-3">
                                <label>Password<span>*</span></label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            <div class="form__btn">
                                <button type="submit" class="btn btn-sm btn-warning btn-block mt-4">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Event Section End -->
<!--
        
<?php $this->load->view('components/common/footer'); ?>
