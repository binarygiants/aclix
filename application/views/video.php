<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>


<!-- Video Section Begin -->
<section class="videos spad">
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title center-title">
                    <h2>Youtube feed</h2>
                    <h1>Latest videos</h1>
                </div>
                <div class="videos__large__item set-bg" data-setbg="<?php echo base_url('').$mainVideo[0]->image; ?>">
                    <a href="<?php echo $mainVideo[0]->link; ?>?autoplay=1" class="play-btn video-popup"><i class="fa fa-play"></i></a>
                    <div class="videos__large__item__text">
                        <h4><?php echo $mainVideo[0]->title; ?>
                        </h4>
                        <ul>
                            <li><?php echo $mainVideo[0]->date; ?></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="videos__slider owl-carousel">
                        <?php $i=0; foreach($videos as $v): if($i>0): ?>
                            <div class="col-lg-3">
                                <div class="videos__item">
                                    <div class="videos__item__pic set-bg" data-setbg="<?php echo base_url('').$v->image; ?>">
                                        <a href="<?php echo $v->link; ?>?autoplay=1" class="play-btn video-popup"><i class="fa fa-play"></i></a>
                                    </div>
                                    <div class="videos__item__text">
                                        <h5><?php echo $v->title; ?></h5>
                                        <ul>
                                            <li><?php echo $v->date; ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php endif; $i++; endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Video Section End -->

<?php $this->load->view('components/common/footer'); ?>