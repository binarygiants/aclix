<!-- Services Section Begin -->
<section class="services">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 p-0">
                <div class="services__left set-bg" data-setbg="<?php echo base_url('assets/template/'); ?>img/service-home.jpeg">
                </div>
            </div>
            <div class="col-lg-6 p-0">
                <div class="row services__list">
                    <div class="col-lg-6 p-0 order-lg-1 col-md-6 order-md-1">
                        <div class="service__item deep-bg">
                            <img src="<?php echo base_url('assets/template/'); ?>img/services/service-1.png" alt="">
                            <h4>Wedding</h4>
                            <p>Make your special day a memorable one! With radio pazhasu. Speak to us for special packages.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0 order-lg-2 col-md-6 order-md-2">
                        <div class="service__item">
                            <img src="<?php echo base_url('assets/template/'); ?>img/services/service-3.png" alt="">
                            <h4>Birthdays & get-togethers</h4>
                            <p>Gift a special moment for your loved ones. Radio pazhasu will take charge of making the moment magical</p>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0 order-lg-4 col-md-6 order-md-4">
                        <div class="service__item deep-bg">
                            <img src="<?php echo base_url('assets/template/'); ?>img/services/service-4.png" alt="">
                            <h4>The journey of love</h4>
                            <p>Not only feed their hunger but you can  also heal their souls with music. Sponsor a musical day for the needy.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0 order-lg-3 col-md-6 order-md-3">
                        <div class="service__item">
                            <img src="<?php echo base_url('assets/template/'); ?>img/services/service-2.png" alt="">
                            <h4>Corporate events</h4>
                            <p>Let them relax, let them have some chill time. Radio pazhasu  will take care of the stress.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services Section End -->