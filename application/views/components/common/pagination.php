<div class="p-0 py-5 w-100 text-right">
    <nav aria-label="Page navigation">
    <ul class="wn__pagination">
        <ul class="pagination justify-content-center p-0" id="pagination" style="font-size:20px;">
            <?php foreach ($pagination['buttons'] as $button ):?>
                <li <?php if($button['status']=="current"): echo 'class="pagination_active"'; endif; ?>>
                    <a href="#" data-id="<?php echo $button['id']; ?>" onclick="getPaginationData(this);">
                        <?php echo $button['value']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </nav> 
</div>
<input type="hidden" id="limit" value="<?php echo $pagination['limit'];?>" />
<input type="hidden" id="cnt" value="<?php echo $pagination['count'];?>" />

      
<script>
    function getPaginationData(id){
        var amount = $('#amount').val();            
        var category = $('#category').val();        
		var condition = $('#condition').val();
        var amounts = amount.split(" - ");
        var min = amounts[0].replace("Rs.", "");
        var max = amounts[1].replace("Rs.", "");
        var base_url = "<?php echo base_url(); ?>"; 
        var pageNo = $(id).attr('data-id'); 
        console.log(min, max, category, pageNo)
        $.ajax({
            type:'GET',
            url: '<?php echo base_url('filterBooks') ?>', 
            data: {min: min, max: max, category: category, condition: condition, pageNo: pageNo},
            dataType: 'json',
            success: function(results){ 
                console.log(results);
                
                var books =""; 
                jQuery.each(results['books'], function( key, val ) {
                    books = books + '<div class="col-lg-3 product product__style--3">';
                    books = books + '<div class="product__thumb">';
                    books = books + '<a href="' + base_url + '/shop/single/' + val['id'] + '">';
                    books = books + '<div style="background-image: url(' + base_url + '/' + val['cover'] + ');" class="book-cover-shop"></div>';
                    books = books + '</a>';
                    books = books + '</div>';
                    books = books + '<div class="product__content content--center">';
                    books = books + '<h4>';
                    books = books + '<a href="' + base_url + '/shop/single/' + val['id'] + '">';
                    books = books + val['title'];
                    books = books + '</a>';
                    books = books + '</h4>';
                    books = books + '<ul class="prize d-flex">';
                    books = books + '<li>Rs. ' + val['price'] + '</li>';
                    books = books + '</ul>';
                    books = books + '<div class="action">';
                    books = books + '<div class="actions_inner">';
                    books = books + '<ul class="add_to_links">';
                    books = books + '<li>';
                    books = books + '<a class="cart" href="' + base_url + '/shop/single/' + val['id'] + '">';
                    books = books + '<i class="bi bi-search"></i>';
                    books = books + '</a>';
                    books = books + '</li>';
                    books = books + '</ul>';
                    books = books + '</div>';
                    books = books + '</div>';
                    books = books + '</div>';
                    books = books + '</div>';
                });
                $('#books').html(books);
                
                var pagination =""; 
                jQuery.each(results.pagination.buttons, function( key, val ) {

                    if(val['status']=="current"){
                        pagination = pagination + '<li class="active">';
                        }
                    else{
                        pagination = pagination + '<li>';
                    }
                    pagination = pagination + '<a href="#" data-id="' + val['id'] + '" onclick="getPaginationData(this); updatePagination(this);">';
                    
                    pagination = pagination + val['value']; 
                    pagination = pagination + '</a>';
                    pagination = pagination + '</li>';
                });
                $('#pagination').html(pagination);
                
                if(results.pagination.count<12){
                    var limit = results.pagination.count;
                }else{
                    var limit = 12;
                }
                $('#currentTab').html('1-'+limit);
                $('#total').html(results.pagination.count);
            },
        
            error:function(){
                console.log('error');
            }
            
        });
    }
</script>