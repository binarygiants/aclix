    <!-- Countdown Section Begin 
    <section class="countdown spad set-bg" data-setbg="<?php echo base_url('assets/template/'); ?>img/countdown-bg.jpg">
        
    </section>
    
    Countdown Section End -->

    <!-- Footer Section Begin -->
    <div id="footer" class="text-center">
    <div class="container">
      <div class="socials-media text-center">

        <ul class="list-unstyled">
          <li><a href="#"><i class="ion-social-facebook"></i></a></li>
          <li><a href="#"><i class="ion-social-twitter"></i></a></li>
          <li><a href="#"><i class="ion-social-instagram"></i></a></li>
          <li><a href="#"><i class="ion-social-googleplus"></i></a></li>
          <li><a href="#"><i class="ion-social-tumblr"></i></a></li>
          <li><a href="#"><i class="ion-social-dribbble"></i></a></li>
        </ul>

      </div>

      <p>&copy; Copyrights Folio. All rights reserved.</p>

      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Folio
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>

    </div>
  </div><!-- End Footer -->

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url('assets/template/'); ?>vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>vendor/php-email-form/validate.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>vendor/typed/typed.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>vendor/venobox/venobox.min.js"></script>
  <script src="<?php echo base_url('assets/template/'); ?>vendor/owl.carousel/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url('assets/template/'); ?>js/main.js"></script>

</body>

</html>