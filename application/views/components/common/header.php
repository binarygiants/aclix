<!DOCTYPE html>
<html lang="zxx">

<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="" name="descriptison">
<meta content="" name="keywords">

    <title>Aclix | <?php echo $pageName; ?></title>
    
    <link rel="icon" href="<?php echo base_url('assets/template/'); ?>img/favicon.png" type="image/png" > 
    <link rel="apple-touch-icon" href="<?php echo base_url('assets/template/'); ?>img/apple-touch-icon.png" >
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/template/'); ?>css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/template/'); ?>vendor/hover/hover.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/template/'); ?>vendor/ionicons/css/ionicons.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/template/'); ?>vendor/boxicons/css/boxicons.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/template/'); ?>vendor/venobox/venobox.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/template/'); ?>vendor/owl.carousel/assets/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/template/'); ?>css/style.css" type="text/css">
    
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>