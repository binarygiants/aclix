    
    <nav id="main-nav">
    <div class="row">
      <div class="container">

        <div class="logo">
          <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/template/'); ?>img/logo.png" alt="logo"></a>
        </div>

        <div class="responsive"><i data-icon="m" class="ion-navicon-round"></i></div>

        <ul class="nav-menu list-unstyled">

    


          <li ><a href="<?php echo base_url(); ?>" class="smoothScroll">Home</a></li>
          <li><a href="<?php echo base_url(); ?>#about" class="smoothScroll">About</a></li>
          <li><a href="<?php echo base_url(); ?>#portfolio" class="smoothScroll">Portfolio</a></li>
          <li><a href="<?php echo base_url(); ?>#journal" class="smoothScroll">Blog</a></li>
          <li><a href="<?php echo base_url(); ?>#contact" class="smoothScroll">Contact</a></li>
        </ul>

      </div>
    </div>
  </nav>
    