<!-- About Section Begin -->
<section class="about spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="about__pic">
                    <img src="<?php echo base_url('assets/template/'); ?>img/niro.png" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about__text">
                    <div class="section-title">
                        <h2>Nirojini Robert</h2>
                        <h1>Founder</h1>
                    </div>
                    <p>We believe soulful music has the magical power of healing. we make the audience sing along with us and enjoy our presence, and that's what differenciates us from the other ordinary bands.</p>
                    <a href="<?php echo base_url('contact'); ?>" class="primary-btn">CONTACT US</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Section End -->