<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>

<div id="hero" class="home">

    <div class="container">
      <div class="hero-content">
        <h1>I'm <span class="typed"></span></h1>
        <p class="typed-items" data-typed-person="Alex Smith">Designer, Developer, Freelancer, Photographer</p>

        <ul class="list-unstyled list-social">
          <li><a href="#"><i class="ion-social-facebook"></i></a></li>
          <li><a href="#"><i class="ion-social-twitter"></i></a></li>
          <li><a href="#"><i class="ion-social-instagram"></i></a></li>
          <li><a href="#"><i class="ion-social-googleplus"></i></a></li>
          <li><a href="#"><i class="ion-social-tumblr"></i></a></li>
          <li><a href="#"><i class="ion-social-dribbble"></i></a></li>
        </ul>
      </div>
    </div>
  </div>

  <?php $this->load->view('components/common/footer'); ?>