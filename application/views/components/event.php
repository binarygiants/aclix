<!-- Event Section Begin -->
<section class="event spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>Our Events</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="event__slider owl-carousel">
                <?php foreach($events as $e): ?>
                    <div class="col-lg-4">
                        <div class="event__item">
                            <div class="event__item__pic set-bg" data-setbg="<?php echo base_url('').$e->image; ?>">
                                <div class="tag-date">
                                    <span><?php echo $e->date; ?></span>
                                </div>
                            </div>
                            <div class="event__item__text">
                                <h4><?php echo $e->title; ?></h4>
                                <p><i class="fa fa-map-marker"></i> <?php echo $e->place; ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<!-- Event Section End -->