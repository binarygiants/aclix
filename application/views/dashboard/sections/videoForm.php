<div class="row">
  <div class="col-6" .mt-2>
    <div class="content-wrapper p-3">
      <?php $this->load->view('dashboard/sections/error') ?>
      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-lg-12">
            <!-- general form elements -->
            <div class="card card-primary">
            <div class="card-header">
             <h3 class="card-title">Video Form</h3>
            </div>         
            <!-- form start -->
            <form action="<?php echo base_url('admin/video/'.$action.'/');?><?php echo $updateData->id ?>" method="POST" name="addForm" enctype="multipart/form-data">
              <div class="card-body">
                <div class="form-group">
                  <label for="inputTitle">Title</label>
                  <input type="text" class="form-control" placeholder="Enter Title" name='title' value="<?php echo $updateData->title; ?>" required>
                </div>
                <div class="form-group">
                  <label for="inputTitle">Date</label>
                  <input type="date" class="form-control" placeholder="Enter Date" name='date' value="<?php echo $updateData->date; ?>" required>
                </div>
                <div class="form-group">
                  <label for="inputTitle">Link</label>
                  <input type="text" class="form-control" placeholder="Enter Link" name='link' value="<?php echo $updateData->link; ?>" required>
                </div>
                <div class="form-group">
                  <label for="inputTitle">Image</label>
                  <input type="file" name="image" id="image" class="form-control" <?php if(!$updateData->id): echo "required"; endif; ?>/>
                </div>
                </div>
              <!-- /.card-body -->       
              <?php  if($action == 'update') { ?>
                <div class="card-footer">
                  <button type="submit"  class="btn btn-primary btn-lg btn-block" name="update">Update</button>
                </div>
                <?php }else { ?>
                <div class="card-footer">
                  <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                </div>
              <?php } ?>                
            </form>
          </div>
        </div>        
      </section>
    </div>
  </div>

